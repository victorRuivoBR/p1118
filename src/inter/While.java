/*
 * Copyright (C) 2018 vcarvalho
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package inter;

import symbols.*;

/**
 *
 * @author vcarvalho
 */
public class While extends Stmt {

    Expr expr;
    Stmt stmt;

    public While() {
        expr = null;
        stmt = null;
    }

    public void init(Expr x, Stmt s) {
        expr = x;
        stmt = s;

        if (expr.type != Type.Bool) {
            expr.error("boolean required in while");
        }
    }

    public void gen(int b, int a) {
        after = a;  // save label a

        expr.jumping(0, a);

        int label = newlabel(); //label for stmt

        emitlabel(label);

        stmt.gen(label, b);

        emit("goto L" + b);
    }
}
